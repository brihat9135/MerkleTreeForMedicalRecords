import java.io.File;
import java.util.Collections;
import java.util.Hashtable;
import java.util.LinkedList;

/***
 * Test class to test the program
 * Test class goes through the directory of the patient files and creates merkleBlock for each and everyday of transactions
 * @author brihat
 *
 */

public class Test {
	
	public static void main(String[] args) {
		
		long startTime = System.nanoTime();
		
		PatientList patientRefrenceList = new PatientList(); 
		Hashtable<String, MerkleBlock> merkleblockListH = new Hashtable<String, MerkleBlock>();
		Hashtable<String, MerkleTree> merkleTreeListH = new Hashtable<String, MerkleTree>();
		LinkedList<MerkleBlock> merkleblockList = new LinkedList<MerkleBlock>();
		LinkedList<MerkleTree> merkleTreeList = new LinkedList<MerkleTree>();
		
		LinkedList<File> folderDate = new LinkedList<File>(); 
		File[] files = new File("Data/").listFiles();
		for(int i = 0; i < files.length; i++) {
			folderDate.add(files[i]);	
		}	
		
		Collections.sort(folderDate);
		for(int i = 0; i < folderDate.size(); i++) {
			String s = folderDate.get(i).getName();
			String directory = "Data/" + s + "/";
			String date = s.substring(4, s.length() - 2) + "/" + s.substring(6, s.length()) + "/" + s.substring(0, s.length() - 4);
			String checkIntegrityPath = "IntegrityCheckData/";
			//System.out.println();
			//System.out.println("Block: " + i);
			//System.out.println("ChartDate: " + date);
			//System.out.println("Directory: " + directory);
			MerkleBlockBuilder MBD = new MerkleBlockBuilder(date, directory, patientRefrenceList, merkleblockList, checkIntegrityPath);
			//System.out.println(MBD.getMerkleBlock());
			merkleblockList.add(MBD.getMerkleBlock());
			merkleTreeList.add(MBD.getMerkleTree());
			merkleblockListH.put(date, MBD.getMerkleBlock());
			merkleTreeListH.put(date, MBD.getMerkleTree());
		}
		
		
		
		
/***	
		
		System.out.println();
		System.out.println("List of MerkleBlock :");
		System.out.println();
		for(int i = 0; i < merkleblockList.size(); i++) {
			if(i == 0) {
				System.out.println("Genesis Block");
			}
			else {
				System.out.println("MerkleBlock Number :" + i);
			}
			System.out.println("MerkleBlock Data: " + merkleblockList.get(i).givendate);
			System.out.println("MerkleBlockHash: " + merkleblockList.get(i).hash);
			System.out.println("MerkleRootHash: " + merkleblockList.get(i).MerkleRootHash);
			
			System.out.println("MerkleTree: " + merkleTreeList.get(i));
			System.out.println("MerkleLeafPatientIDs: " + merkleTreeList.get(i).MLF.getPatientID());
			System.out.println();
		}
		
		
**/	
		
			//System.out.println("MerkleBlockList with Date: " + merkleblockListH);
			//System.out.println(patientRefrenceList.getLatestNode(1453).PreviousNode);
			//patientRefrenceList.getAllNodes(2507);
			//System.out.println(patientRefrenceList.getLatestNode(1453).PreviousNode.Parent.Parent.Parent.Parent.Parent.gethashString());
			//System.out.println();
			
		
			patientRefrenceList.getAllNodes(2507);                   // gives all the node for particular ID
			//patientRefrenceList.printList();		
			System.out.println();
			System.out.println("Total MerkleBlocks: " + merkleblockList.size());
			System.out.println();
			System.out.println("MerkleBlock Genesis: " + merkleblockListH.get("01/01/2151"));
			System.out.println("MerkleBlock Genesis Date: " + merkleblockListH.get("01/01/2151").givendate);
			//System.out.println(patientRefrenceList.getLatestDate(1453));
			System.out.println(merkleTreeListH.get("01/01/2151").merkleLeaf.getPatientID());
			System.out.println("Patient count :" + merkleTreeListH.get("01/01/2151").merkleLeaf.getPatientID().size());
			System.out.println();
			System.out.println("MerkleBlock: " + merkleblockListH.get("01/02/2152"));
			System.out.println("MerkleBlock: " + merkleblockListH.get("01/02/2152").givendate);
			System.out.println(merkleTreeListH.get("01/02/2152").merkleLeaf.getPatientID());
			
			System.out.println();
			
			long endTime = System.nanoTime();
			long totalTime = endTime - startTime;
			double seconds = (double)totalTime/1000000000.0;
			System.out.println("Total time taken to run the program :" + seconds + " sec");
	
	}	
}
