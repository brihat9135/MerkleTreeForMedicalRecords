import java.util.*;

/**
 * PatientList contains patientID with corresponding MerkleNode with Date
 * This class in combine with MerkleNode class is used to create MerkleNode reference 
 * @author brihat
 *
 */


public class PatientList {
	public Hashtable<String, LinkedList<PatientNodeInfo>> patientRefrence;
	
	public PatientList() {
		this.patientRefrence = new Hashtable<String, LinkedList<PatientNodeInfo>>();
	}
	
	public void addPatient(String patientID, PatientNodeInfo Nodeinfo) {
		if(this.patientRefrence.isEmpty()) {
			LinkedList<PatientNodeInfo> L1 = new LinkedList<PatientNodeInfo>();
			L1.add(Nodeinfo);
			this.patientRefrence.put(patientID, L1);
		}
		else {
			if(this.patientRefrence.containsKey(patientID)) {
				//System.out.println("Found patientID");
				this.patientRefrence.get(patientID).add(Nodeinfo);
			}
			else {
				LinkedList<PatientNodeInfo> L2 = new LinkedList<PatientNodeInfo>();
				L2.add(Nodeinfo);
				this.patientRefrence.put(patientID, L2);
			}
		}
	}
	
	/**
	 * Prints all the patientID with MerkleNode 
	 */
	
	public void printList() {
		System.out.println();
		System.out.println("Total Patient Numbers: " + this.patientRefrence.size());
		for (Map.Entry<String, LinkedList<PatientNodeInfo>> entry : this.patientRefrence.entrySet()) {
			String patientID = entry.getKey();
			LinkedList<PatientNodeInfo> Nodelist = entry.getValue();
			for (int i = 0; i < Nodelist.size(); i++) {
				System.out.println("PatientID : " + patientID + "   Node : " + Nodelist.get(i).Node + "   Date : " + Nodelist.get(i).Node.date + "   PreviousNode : " + Nodelist.get(i).Node.PreviousNode);
			}
			
		}
	}
	
	/**
	 * getLatestNode will give latestNode created for that particular patient ID
	 * @param ID
	 * @return
	 */
	
	public MerkleNode getLatestNode(int ID) {
		LinkedList<PatientNodeInfo> PatientNodeInfo = patientRefrence.get(Integer.toString(ID));
			return PatientNodeInfo.getLast().Node;
	}
	
	
	/**
	 * This method help to get all the Nodes given patient ID
	 * @param ID
	 */
	public void getAllNodes(int ID) {
		LinkedList<PatientNodeInfo> PatientNodeInfo = patientRefrence.get(Integer.toString(ID));
		MerkleNode Node = PatientNodeInfo.getLast().Node;
		while(Node != null) {
			System.out.println(Node.date + " "+ Node);
			Node = Node.PreviousNode;
		}
	}
	
	/**
	 * This method return last date when the patient information was written
	 * @param ID
	 * @return
	 */
	public String getLatestDate(int ID) {
		LinkedList<PatientNodeInfo> PatientNodeInfo = patientRefrence.get(Integer.toString(ID));
		return PatientNodeInfo.getLast().date;
	}
}
