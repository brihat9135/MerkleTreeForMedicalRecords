
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


/***
 * MerkleBlock create hash combining previous block hash, date and MerkleRoot Hash 
 * This is the building block of the blockchain, which create a block for one day of transaction 
 * SHA-256 is used to create hash for each block 
 * @author brihat
 *
 */

public class MerkleBlock {
	public String MerkleRootHash;
	public String Previoushash;
	public String givendate;
	public String hash;
	
	
	public MerkleBlock(String previoushash, String date, String merkleRootHash) {
		this.Previoushash = previoushash;
		this.givendate = date;
		this.MerkleRootHash = merkleRootHash;
		try {
			this.hash = createHash();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
	
	/***
	 * CreateHash method combines date, MerkleTree Root hash, and previous block hash
	 * Hashes the result of combines bytes, which will be used on the next MerkleBlock as previous hash 
	 * @return combinedHash
	 * @throws UnsupportedEncodingException
	 */
	
	public String createHash() throws UnsupportedEncodingException {
		byte[] dbytes = givendate.getBytes();
		byte[] mbytes = MerkleRootHash.getBytes();
		if(Previoushash != null) {
			byte[] pbytes = Previoushash.getBytes();
			byte[] combinebytes = new byte[pbytes.length + dbytes.length + mbytes.length];
			System.arraycopy(pbytes, 0, combinebytes, 0, pbytes.length);
			System.arraycopy(dbytes, 0, combinebytes, pbytes.length, dbytes.length);
			System.arraycopy(mbytes, 0, combinebytes, dbytes.length, mbytes.length);
			String combinedHash = bytetoHashCovert(combinebytes);
			return combinedHash;
		}
		else {
			byte[] combinebytes = new byte[dbytes.length + mbytes.length];
			System.arraycopy(dbytes, 0, combinebytes, 0, dbytes.length);
			System.arraycopy(mbytes, 0, combinebytes, dbytes.length, mbytes.length);
			String combinedHash = bytetoHashCovert(combinebytes);
			return combinedHash;
		}
		
	}
	
	
	
	public String bytetoHashCovert(byte[] filename) {
		String convertedHash = null;
		try {
        	MessageDigest md = MessageDigest.getInstance("SHA-256");
        	byte[] key=md.digest(filename);
        	convertedHash = byteTostring(key);
        	}
        catch (NoSuchAlgorithmException e) {
        	e.printStackTrace();
        }
		return convertedHash;
	}
	
	
	
	public String byteTostring(byte[] b) {
		StringBuffer buf = new StringBuffer();
	    for (int i = 0; i < b.length; i++) {
	    	buf.append(Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1));
	     }
	       return buf.toString();
	}
	
	
	public String getMerkleBlockHash() {
		return this.hash;
	}
	
	
	public String getMerkleRootHash( ) {
		return this.MerkleRootHash;
	}
	
}
