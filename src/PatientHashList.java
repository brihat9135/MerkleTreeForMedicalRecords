import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;


/***
 * PatientHashList hashes the patient data and makes them ready for MerkleTree class to create merkleTree
 * This class also checks for integrity immediately when it creates merkleTree 
 * @author brihat
 *
 */

public class PatientHashList{
	
	public Map<String, byte[]> patientHashes;					//hash with bytes, keep null if no data available
	public LinkedList<byte[]> patientListBytes;
	public LinkedList<String> patientStrHash;                     // needed to create the initial layer
	public Map<String, String> patientHashList;   
	public MerkleNode merkleRoot;
	public String date;
	public LinkedList<String> patientIDList;
	public String integrityPath;
	private BufferedReader bufferedReader;
	public File[] files;
	public int countNumberofFiles;
	public boolean filefound;
	
	
	public PatientHashList(String date, MerkleLeaf ML, String path) {
		this.patientHashList = Collections.synchronizedMap(new LinkedHashMap<String, String>());
		this.patientHashes = Collections.synchronizedMap(new LinkedHashMap<String, byte[]>());
		this.patientListBytes = ML.listofbyte;
		this.patientIDList = ML.patientIDList;
		this.date = date;
		this.integrityPath = path;
		this.merkleRoot = new MerkleNode(null, null, null, null, this.date);
		File file = new File(integrityPath);
		files = file.listFiles();
		this.countNumberofFiles = 0;
		this.filefound = false;
	}
	
	
	/***
	 * CreateInitialLayer hashes the bytes of each patient and create the initial layer of the merkleTree
	 * The hashes are kept in a map and makes it ready for the next layer of the tree
	 * @return
	 */
	public Map<String, byte[]> createInitialLayer() {
		String filehashes = null;
		LinkedList<String> patientStringHash = new LinkedList<String>();
		//System.out.println("Total Files:" + patientListBytes.size());
		for (int i = 0; i < patientListBytes.size(); i++) {
			filehashes = bytetoHashCovert(patientListBytes.get(i));
			this.patientHashes.put(filehashes, patientListBytes.get(i));
			patientStringHash.add(filehashes);
		}
		this.patientStrHash = patientStringHash;
		return patientHashes;
		
	}
	
	/***
	 * CreateAllHashes takes hashes created from the bytes of patients, pairs them and concatenate to create layer for the markleTree
	 * This process is done recursively to get the final root of the tree
	 * @param listofpatients
	 * @return patientHashes
	 */
	
	public Map<String, byte[]> CreateAllHashes(LinkedList<String> listofpatients) {
		LinkedList<String> patientList = listofpatients;
		LinkedList<String> temp_list = new LinkedList<String>(); 
		String a = null;
		String b = null;	
		if(patientList.size() > 1) {
			for(int i = 0; i < patientList.size(); i = i + 2) {
				//System.out.println("Patientlist size:" + patientList.size());
				a = patientList.get(i);
				String changetoString = null;
				if(i + 1 < patientList.size()) {
					b = patientList.get(i+1);
					String combineHash = null;
					combineHash = a + b;
					changetoString = hashConvert(combineHash);
					this.patientHashList.put(combineHash, changetoString);
					this.patientHashes.put(changetoString, null);
					temp_list.add(changetoString);
				}
				else {
					changetoString = hashConvert(a);
					this.patientHashList.put(a, changetoString);
					this.patientHashes.put(changetoString, null);
					temp_list.add(changetoString);
				}	
			}
			return CreateAllHashes(temp_list);	
		}
		return this.patientHashes;
	}
		
	
	/***
	 * Converts bytes to string
	 * @param b
	 * @return
	 */
	
	public String byteTostring(byte[] b) {
		StringBuffer buf = new StringBuffer();
	    for (int i = 0; i < b.length; i++) {
	    	buf.append(Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1));
	     }
	       return buf.toString();
	}
	
	
	/***
	 * This method converts string to hash
	 * @param name
	 * @return changetoString (string 
	 */
	
	public String hashConvert(String name) {
		String newString = null;
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			
			md.update(name.getBytes(StandardCharsets.UTF_8));
			byte[] key=md.digest();
			newString = byteTostring(key);
		}
		catch (NoSuchAlgorithmException e) {
        		e.printStackTrace();
		}
		return newString;
	}
	
	
	public String bytetoHashCovert(byte[] filename) {
		String changetoString = null;
		try {
        	MessageDigest md = MessageDigest.getInstance("SHA-256");
        	byte[] key=md.digest(filename);
        	changetoString = byteTostring(key);
        	//System.out.println("hash : " + changetoString);
        	}
        catch (NoSuchAlgorithmException e) {
        	e.printStackTrace();
        }
		return changetoString;
	}
	
	
	
	/***
	 * Method to checkIntegrity, which goes inside the directory consisting of the txt file for each merkletree hashes
	 * Compares the root. If the roots are same, it passes. If not, then check hashes for the patient file bytes to find the
	 * patient whose information has been tampered
	 * @throws FileNotFoundException
	 * @throws UnsupportedEncodingException
	 */
	public void checkingIntegrity() throws FileNotFoundException, UnsupportedEncodingException {
		
		String nDate = date.substring(6, date.length()) + date.substring(0, date.length() - 8) + date.substring(3, date.length() - 5);
		if(files.length > 0) {
					
			for(int i = 0; i < files.length; i++) {

				String filename = files[i].getName().substring(0, files[i].getName().length() - 4);
				if(filename.equals(nDate)) {
					filefound = true;
					try {
						bufferedReader = new BufferedReader(new FileReader(integrityPath + files[i].getName()));
						String line;
						Set<String> hashe = patientHashes.keySet();
						List<String> listhashe = new ArrayList<String>(hashe);
						Collections.reverse(listhashe);
						Stack<String> stacklistofhashes = new Stack<String>();
						boolean integrityPassed = false;
						while((line = bufferedReader.readLine()) != null) {
							if(line.equals(listhashe.get(0))){
								integrityPassed = true;
								break;
							}
							else {
								stacklistofhashes.push(line);   //adding the hashes from the txt file to stack
							}
						}
						
						if(integrityPassed == false) {
							int count = 0;
							for(String s : hashe) {
								String b = stacklistofhashes.pop().toString();
								if(s.equals(b)){
									count += 1;
									continue;
								}
								else {
									System.out.println("IntegrityFailed!!!!");
									System.out.println("failed hash: " + b);
									System.out.println("Date: " + this.date);
									System.out.println("failed patientID: " + this.patientIDList.get(count));
									System.exit(0);
								}
								count += 1;
							}	
						}
						
					} catch (IOException e) {
						
						e.printStackTrace();
					}
				}
				else {
					continue;
				}
			}
				if(filefound == false) {
					Set<String> hashes = patientHashes.keySet();
					List<String> listhashes = new LinkedList<String>(hashes);
					Collections.reverse(listhashes);
					
					
					PrintWriter write = new PrintWriter(integrityPath + nDate + ".txt", "UTF-8");
					for (String s : listhashes) {
						write.println(s);
					}
					write.close();
				}
			}
		else {
			Set<String> hashes = patientHashes.keySet();
			List<String> listhashes = new LinkedList<String>(hashes);
			Collections.reverse(listhashes);
			PrintWriter write = new PrintWriter(integrityPath + nDate + ".txt", "UTF-8");
			for (String s : listhashes) {
				write.println(s);
			}
			write.close();
		}	
	}
}
