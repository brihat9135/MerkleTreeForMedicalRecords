
/***
 * PatientNodeInfo consists of MerkleNode and date
 * This class is used in PatientList to add patient with patient ID and LinkedList of PatientNodeInfo
 * @author brihat
 *
 */

public class PatientNodeInfo {
	public MerkleNode Node;
	public String date;
	
	PatientNodeInfo(MerkleNode Node, String date){
		this.Node = Node;
		this.date = date;
	}
	
	
	public MerkleNode getNode() {
		return this.Node;
	}
	
	public String getDate() {
		return this.date;
	}
}
