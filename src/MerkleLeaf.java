import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;


/***
 * MerkleLeaf reads the patient's information, converts to bytes and keeps in a list
 * MerkleLeaf is the base layer of the MerkleTree
 * This class contains getID method, which is used in MerkleTree class to get patientID
 * This class needs file directory to convert them into bytes and make them ready for PatientHashList class
 * @author brihat
 *
 */

public class MerkleLeaf { 
	public LinkedList<File> fileList;
	public LinkedList<String> patientIDList;
	public String filedirectory;
	public LinkedList<byte[]> listofbyte;
	
	
	public MerkleLeaf(String FileDirectory) {
		this.fileList = new LinkedList<File>();
		this.patientIDList = new LinkedList<String>();
		this.filedirectory = FileDirectory;
		this.listofbyte = new LinkedList<byte[]>();
	}
	
	/***
	 * getListFile gets the name of the file and keeps them in a list
	 * this also get the patientID and keeps them in a list
	 * @return
	 */
	
	public LinkedList<File> getListFile() {
		File[] files = new File(this.filedirectory).listFiles();
		for(int i = 0; i < files.length; i++) {
			fileList.add(files[i]);
			String s = files[i].getName();
			String patientID = s.substring(0, s.length() - 4);
			patientIDList.add(patientID);
		}
		return fileList;
	}
	
	
	
	public LinkedList<String> getPatientID() {
		return this.patientIDList;	
	}
	
	
	
	public int getSizeofPatientList() {
		return patientIDList.size();
	}
	
	
	
	public String byteTostring(byte[] b) {
		StringBuffer buf = new StringBuffer();
	    for (int i = 0; i < b.length; i++) {
	    	buf.append(Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1));
	     }
	       return buf.toString();
	   }
	
	
	/**
	 * getListofFileBytes converts files into bytes and keeps them in a list
	 * @return listofBytes
	 */
	
	public LinkedList<byte[]> getListofFilebytes() {
		for(int i = 0; i < this.fileList.size(); i++) {
			File oneFile = this.fileList.get(i);
			FileInputStream fin = null;
			byte[] fileContent = null;
			try {

				fin = new FileInputStream(oneFile);
				fileContent = new byte[(int)oneFile.length()];
			}
			catch (FileNotFoundException e) {
				System.out.println("File not found" + e);
			}
			finally {
				try {
					if (fin != null) {
						fin.close();
					}
				}
				catch (IOException ioe) {
					System.out.println("Error while closing stream: " + ioe);
				}
			}
			this.listofbyte.add(fileContent);
		}
		return listofbyte;
	}
	
	
	/***
	 * This method provides patientID if it exist for that particular hash, otherwise gives null
	 * @param i
	 * @return patientID
	 */
	
	public String getID(int i) {
		if(i >= this.patientIDList.size()) {
			return null;
		}
		else {
			return patientIDList.get(i);
		}
	}	
}
