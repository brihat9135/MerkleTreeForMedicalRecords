import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.LinkedList;

/***
 * MerkleBlockBuilder does the one day transaction all at once
 * This class takes all the variables required to create MerkleLeaf, MerkleTree, MerkleNode, PatientHashList and PatientList
 * Everything is done in the constructor
 * @author brihat
 *
 */

public class MerkleBlockBuilder {
	public String date;
	public String directory;
	public MerkleLeaf merkleLeaf;
	public PatientHashList patientHashList;
	public MerkleTree merkleTree;
	public PatientList patientList;
	public MerkleBlock merkleBlock;
	public LinkedList<MerkleBlock> merkleBlockList;
	
	
	
	public MerkleBlockBuilder(String Date, String directory, PatientList patientl, LinkedList<MerkleBlock> merklebList, String integrityCheckPath) {
		this.date = Date;
		this.directory = directory;
		this.merkleLeaf = new MerkleLeaf(this.directory);
		this.merkleLeaf.getListFile();
		this.merkleLeaf.getListofFilebytes();
		this.patientHashList = new PatientHashList(Date, merkleLeaf, integrityCheckPath);
		this.patientHashList.createInitialLayer();
		this.patientHashList.CreateAllHashes(patientHashList.patientStrHash);
			
		
		try {
			patientHashList.checkingIntegrity();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		this.patientList = patientl;
		this.merkleTree = new MerkleTree(patientHashList, patientList, Date, merkleLeaf);
		this.merkleTree.buildMerkleTree();
		this.merkleBlockList = merklebList;
		
		if(!merkleBlockList.isEmpty()) {
			this.merkleBlock = new MerkleBlock(merkleBlockList.getLast().getMerkleBlockHash(), Date, merkleTree.merkleRoot.gethashString());
		}
		else {
			this.merkleBlock = new MerkleBlock(null, Date, merkleTree.merkleRoot.gethashString());
		}
	}
	
	
	
	public MerkleBlock getMerkleBlock() {
		return this.merkleBlock;	
	}
	
	public MerkleNode getMerkleRootNode() {
		return this.merkleTree.merkleRoot;
	}
	
	public MerkleTree getMerkleTree() {
		return this.merkleTree;
	}

	
}
