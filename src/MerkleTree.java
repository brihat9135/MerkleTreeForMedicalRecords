
import java.util.*;



/***
 * MerkleTree class takes PatientHashList and create MerkleTree dynamically 
 * MerkleTree also create each and every merkleNode and connects them with other node to create balanced binary tree
 * MerkleTree required patientHashList, which contains all the patienthashes including the upper layers of hashes
 * PatientList from the patientList class, which is used to provide refrence to each patient Node in the base layer of the tree
 * LinkedList of patients files(txt files)
 * @author brihat
 *
 */

public class MerkleTree {
	
	public PatientHashList patientHashList;
	public MerkleNode merkleRoot;
	public int templayerSize;                               // this dynamically changing int value determines the size of the MerkleTree layers
	public LinkedList<String> patienthash;					// all the hashes
	public LinkedList<byte[]> patientdata;					// all the byte[] but null if there is none
	public MerkleNode tempNode;								// this dynamically changing node help to create nodes for each patient and connect to other nodes
	public MerkleLeaf merkleLeaf;
	public Deque<MerkleNode> tempDeque;						// this dynamically changing deque will help to create right and left child node while creating layers in merkleTree
	public PatientList patientList;							// this is needed when creating merkleNode
	public int tempPosition;								// this help to determine the position number of patient we have reached after each layer	
	public int numberofNodesCreated;						// this counts the number of node MerkleTree has created, which helps for the while loop in the recursion
	public String date;
	
	public MerkleTree(PatientHashList patientHashList, PatientList plist, String date, MerkleLeaf merkleLeaf) {
		this.patientHashList = patientHashList;
		this.patienthash = new LinkedList<String>(patientHashList.patientHashes.keySet());
		this.patientdata = new LinkedList<byte[]>(patientHashList.patientHashes.values());
		this.date = date;
		this.merkleRoot = new MerkleNode(null, null, null, null, this.date);
		this.templayerSize = merkleLeaf.listofbyte.size();
		this.patientList = plist;
		this.merkleLeaf = merkleLeaf;
		this.tempDeque = new ArrayDeque<MerkleNode>();
		this.tempPosition = 0;
		this.numberofNodesCreated = 0;
		
	}
	
	
	public MerkleNode buildMerkleTree() {
		//System.out.println("Patienthash size: " + patienthash.size());		
		int b = tempPosition;
		int c = 0;
		for (int i = b; i < patienthash.size(); i = i + c) {

			Deque<MerkleNode> Nodelist = new ArrayDeque<MerkleNode>();
			while(numberofNodesCreated < templayerSize) {	
				if(this.patientdata.get(i) != null) {
					MerkleNode node = new MerkleNode(this.patienthash.get(i + numberofNodesCreated), this.patientdata.get(i + numberofNodesCreated), 
							this.patientList, this.merkleLeaf.getID(i + numberofNodesCreated), this.date);
					node.addRefrence();                                   // adding the Node to the PatientList, which will also give reference to previous node
					Nodelist.add(node);
				}
				else {
					MerkleNode node = new MerkleNode(this.patienthash.get(i + numberofNodesCreated), this.patientdata.get(i + numberofNodesCreated), 
							this.patientList, this.merkleLeaf.getID(i + numberofNodesCreated), this.date);
					Nodelist.add(node);
					this.tempNode = tempDeque.removeFirst();
					node.LeftChild = tempNode;
					tempNode.Parent = node;
					if(!tempDeque.isEmpty()) {
						this.tempNode = tempDeque.removeFirst();
						node.RightChild = tempNode;
						tempNode.Parent = node;
					}	
				}	
			numberofNodesCreated++;
			}
			this.tempDeque = Nodelist;
			this.tempPosition = b + numberofNodesCreated;
			b = tempPosition;
			c = numberofNodesCreated;
			numberofNodesCreated = 0;
			if(this.templayerSize % 2 != 0) {
				this.templayerSize = (this.templayerSize+1)/2;
			}
			else {
				this.templayerSize = (this.templayerSize/2);
			}	
		}
			this.merkleRoot = tempDeque.removeFirst();
			//System.out.println("MerkleRoot Hash :" + merkleRoot.gethashString());
			return merkleRoot;
	}
	
}	
