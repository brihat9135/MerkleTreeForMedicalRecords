
/***
 * MerkleNode class provides attributes for one typical Node
 * addRefrence method in this class, search the patient in the all the patient records, provides refrence to the previous node
 * and adds the node to the list for quick access
 * @author cs
 *
 */

public class MerkleNode {
	public MerkleNode Parent;
	public MerkleNode LeftChild;
	public MerkleNode RightChild;
	public byte[] patientData;
	public MerkleNode PreviousNode;
	public PatientList listOfPatient;
	public String hashName;
	public String date;
	public String patientID;
	
	
	public MerkleNode(String hashData, byte[] data, PatientList plist, String pID, String date) {
		this.patientData = data;
		this.LeftChild = null;
		this.RightChild = null;
		this.Parent = null;
		this.hashName = hashData;
		this.listOfPatient = plist;
		this.patientID = pID;
		this.PreviousNode = null;
		this.date = date;
		
	}
	
	public void setHashName(String input) {
		this.hashName = input;
	}
	
	public String gethashString() {
		return this.hashName;
	}
	
	
	/***
	 * addRefrence method checks the patientID on the list of patients created in the PatientList class
	 * add to the list, takes the previous node and add it to this node
	 * @return PreviousNode
	 */
	public MerkleNode addRefrence() {
		PatientNodeInfo PNI = new PatientNodeInfo(this, this.date);
		if(listOfPatient.patientRefrence.isEmpty()) {
			listOfPatient.addPatient(this.patientID, PNI);
			this.PreviousNode = null;
		}
		else {
			if(listOfPatient.patientRefrence.containsKey(this.patientID)) { 
				this.PreviousNode = listOfPatient.patientRefrence.get(this.patientID).getLast().Node;
				listOfPatient.addPatient(this.patientID, PNI);
				
			}
			else {
				listOfPatient.addPatient(this.patientID, PNI);
				this.PreviousNode = null;
			}
		}
		return PreviousNode;
	}
	
	public MerkleNode getPreviousNode() {
		return this.PreviousNode;
	}
	
	
	
}
